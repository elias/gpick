Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gpick
Upstream-Contact: Albertas Vyšniauskas <thezbyg@gmail.com>
Source: http://www.gpick.org

Files: *
Copyright: 2009-2020 Albertas Vyšniauskas <thezbyg@gmail.com>
License: BSD-3

Files: share/gpick/color_dictionary_0.txt
Copyright: not-applicable
License: public-domain
 No license required for any purpose; the work is not subject to copyright
 in any jurisdiction.

Files: share/metainfo/gpick.appdata.xml
Copyright: 2009-2016 Albertas Vyšniauskas <thezbyg@gmail.com>
License: CC0-1.0

Files: debian/*
Copyright: 2011-2021 Elías Alejandro Año Mendoza <ealmdz@gmail.com>
License: BSD-3

License: BSD-3
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the software author nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: CC0-1.0
 On Debian systems, the complete text of the Creative Commons CC0 1.0
 Universal license (CC0-1.0) can be found in
 "/usr/share/common-licenses/CC0-1.0".
